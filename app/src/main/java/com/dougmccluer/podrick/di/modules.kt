package com.dougmccluer.podrick.di

import androidx.lifecycle.SavedStateHandle
import com.dougmccluer.podrick.MainActivityViewModel
import com.dougmccluer.podrick.event.IEventBus
import com.dougmccluer.podrick.event.RxBus
import com.dougmccluer.podrick.remote.TypesAdapterFactory
import com.dougmccluer.podrick.remote.XNullableAdapterFactory
import com.dougmccluer.podrick.screens.PodrickBackStackContainer
import com.dougmccluer.podrick.screens.detail.DetailViewFactory
import com.dougmccluer.podrick.screens.detail.DetailWorkflow
import com.dougmccluer.podrick.screens.discover.DiscoverViewFactory
import com.dougmccluer.podrick.screens.discover.DiscoverWorkflow
import com.dougmccluer.podrick.screens.error.ErrorViewFactory
import com.dougmccluer.podrick.screens.loading.LoadingViewFactory
import com.dougmccluer.podrick.screens.shell.ShellViewFactory
import com.dougmccluer.podrick.screens.shell.ShellWorkflow
import com.dougmccluer.podrick.screens.subscriptions.SubscriptionsViewFactory
import com.dougmccluer.podrick.screens.subscriptions.SubscriptionsWorkflow
import com.dougmccluer.podrick.screens.welcome.WelcomeViewFactory
import com.dougmccluer.podrick.util.*
import com.dougmccluer.podrick.views.toolbar.ToolbarWorkflow
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.squareup.workflow1.ui.ViewRegistry
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import de.mfietz.fyydlin.FyydClient
import de.mfietz.fyydlin.FyydService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit


val podsmods = module {
	single<IResourceProvider> {
		ResourceProvider(androidContext())
	}
	single<Moshi> {
		Moshi.Builder()
			.add(XNullableAdapterFactory())
			.add(KotlinJsonAdapterFactory())
			.add(TypesAdapterFactory())
			.build()
	}
	single<IStateSerializer> { PodrickSnapshotter }
	single<IEventBus> { RxBus() }
	single { createOkHttpClient() }
	single { createWebService<FyydService>(okHttpClient = get(), url = "", moshi = get()) }
	single { DetailWorkflow(resourceProvider = get(), snapshotter = get()) }
	single {
		DiscoverWorkflow(
			resourceProvider = get(),
			fyydClient = get(),
			eventBus = get(),
			snapshotter = get()
		)
	}
	single { SubscriptionsWorkflow(resourceProvider = get(), snapshotter = get()) }
	single { FyydClient() }
	single { ToolbarWorkflow() }
	single {
		ShellWorkflow(
			discoverWorkflow = get(),
			toolbarWorkflow = get(),
			detailWorkflow = get(),
			subscriptionsWorkflow = get(),
			resourceProvider = get(),
			eventBus = get(),
			snapshotter = get(),
		)
	}
	single { IImageLoader.getDefaultImplementation() }

	@OptIn(WorkflowUiExperimentalApi::class)
	single {
		ViewRegistry(
			DiscoverViewFactory(imageLoader = get()),
			DetailViewFactory(imageLoader = get()),
			LoadingViewFactory,
			PodrickBackStackContainer,
			ShellViewFactory(),
			WelcomeViewFactory(),
			ErrorViewFactory(),
			SubscriptionsViewFactory()
		)
	}

	viewModel { (handle:SavedStateHandle) -> MainActivityViewModel(handle, get()) }
}

fun createOkHttpClient():OkHttpClient {
	val loggingInterceptor =
		HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

	return OkHttpClient.Builder()
		.readTimeout(60, TimeUnit.SECONDS)
		.connectTimeout(120, TimeUnit.SECONDS)
		.addInterceptor(loggingInterceptor)
		.build()
}

inline fun <reified T> createWebService(
	okHttpClient:OkHttpClient,
	url:String,
	moshi:Moshi
):T {
	val retrofit = Retrofit.Builder()
		.baseUrl(url)
		.client(okHttpClient)
		.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
		.addConverterFactory(
			MoshiConverterFactory.create(moshi).asLenient().withNullSerialization()
		)
		.build()
	return retrofit.create(T::class.java)
}