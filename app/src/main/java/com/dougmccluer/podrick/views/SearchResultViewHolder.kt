package com.dougmccluer.podrick.views

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.dougmccluer.podrick.R
import com.dougmccluer.podrick.databinding.SearchResultViewHolderBinding
import com.dougmccluer.podrick.util.IImageLoader
import com.dougmccluer.podrick.util.firstNonBlank
import com.dougmccluer.podrick.util.setTextOrHide
import de.mfietz.fyydlin.SearchHit

class SearchResultViewHolder(
	private val view:View,
	private val imageLoader:IImageLoader,
):
	RecyclerView.ViewHolder(view) {

	fun bind(item:SearchHit, onSelected:(SearchHit) -> Unit = {}) {
		view.setOnClickListener {
			onSelected(item)
		}

		SearchResultViewHolderBinding.bind(view).apply {
			searchResultTitle.setTextOrHide(item.title)
			searchResultSubtitle.setTextOrHide(item.subtitle)
			searchResultDescription.setTextOrHide(item.description)

			let {
				val placeholder = IImageLoader.makeTextPlaceholder(
					text = item.title,
					radius = itemView.context.resources.getDimensionPixelSize(R.dimen.label_margin),
					textPxSize = (itemView.context.resources.getDimensionPixelSize(R.dimen.thumbnail_size) * .5).toInt()
				)

				imageLoader.loadImage(
					firstNonBlank(
						item.smallImageURL,
						item.thumbImageURL,
						item.imageUrl,
						item.microImageURL,
						item.layoutImageUrl
					),
					searchResultThumbnail,
					placeholder
				)
			}
		}
	}


}