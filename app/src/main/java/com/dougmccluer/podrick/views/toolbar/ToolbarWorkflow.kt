package com.dougmccluer.podrick.views.toolbar

import com.squareup.workflow1.Snapshot
import com.squareup.workflow1.StatefulWorkflow
import com.squareup.workflow1.action

class ToolbarWorkflow
	:StatefulWorkflow<Unit, ToolbarWorkflow.State, ToolbarWorkflow.ToolbarOutput, ToolbarRendering>() {
	sealed class State {
		data class Search(val currentText:String?=null):State()
	}

	sealed class ToolbarOutput {
		data class SearchOutput(val query:String?):ToolbarOutput()
	}

	override fun initialState(props:Unit, snapshot:Snapshot?) = State.Search()

	override fun render(props:Unit, state:State, context:RenderContext):ToolbarRendering {
		return when (state) {
			is State.Search -> ToolbarRendering(
				currentText = state.currentText,
				onQuerySubmitted = { query:String? ->
					context.actionSink.send(
						action {
							setOutput(ToolbarOutput.SearchOutput(query))
						}
					)
					true
				},
				onQueryTextChanged = { newText:String? ->
					context.actionSink.send(action {this.state = state.copy(newText)})
					true
				}
			)
		}
	}

	override fun snapshotState(state:State):Snapshot? {
		return null
	}
}

