package com.dougmccluer.podrick.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dougmccluer.podrick.R
import com.dougmccluer.podrick.util.IImageLoader
import de.mfietz.fyydlin.SearchHit

class SearchResultsRecyclerAdapter(
	private val imageLoader:IImageLoader,
	private val onItemSelected:(SearchHit)->Unit
):RecyclerView.Adapter<SearchResultViewHolder>() {

	private val results:ArrayList<SearchHit> = ArrayList()

	override fun onCreateViewHolder(parent:ViewGroup, viewType:Int):SearchResultViewHolder {
		return SearchResultViewHolder(
			LayoutInflater.from(parent.context).inflate(
				R.layout.search_result_view_holder,
				parent,
				false),
			imageLoader
		)
	}

	fun setData(data:List<SearchHit>){
		results.addAll(data)
		notifyDataSetChanged()
	}

	override fun onBindViewHolder(holder:SearchResultViewHolder, position:Int) {
		results[position].let{
			holder.bind(it, onItemSelected)
		}
	}

	override fun getItemCount() = results.size
}
