package com.dougmccluer.podrick.views.toolbar

import android.content.Context.INPUT_METHOD_SERVICE
import android.inputmethodservice.InputMethodService
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.SearchView
import com.dougmccluer.podrick.databinding.ToolbarBinding
import com.squareup.workflow1.ui.AndroidViewRendering
import com.squareup.workflow1.ui.LayoutRunner
import com.squareup.workflow1.ui.ViewFactory
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi

@OptIn(WorkflowUiExperimentalApi::class)
class ToolbarRendering(
	val currentText:String?,
	val onQuerySubmitted:(String?) -> Boolean,
	val onQueryTextChanged: (String?) -> Boolean
):AndroidViewRendering<ToolbarRendering> {

	override val viewFactory:ViewFactory<ToolbarRendering> =
		LayoutRunner.bind(ToolbarBinding::inflate) { rendering, env ->

			toolbarSearch.setOnQueryTextListener(
				object:SearchView.OnQueryTextListener {
					override fun onQueryTextSubmit(query:String?):Boolean {
						(toolbar.context.getSystemService(INPUT_METHOD_SERVICE)
							as? InputMethodManager)?.let{
							it.hideSoftInputFromWindow(toolbar.windowToken, 0)
						}
						val ret = onQuerySubmitted(query)
						toolbarSearch.setQuery("", false)
						toolbarSearch.clearFocus()
						return ret
					}

					override fun onQueryTextChange(newText:String?):Boolean {
						return onQueryTextChanged(newText)
					}
				}

			)
		}
}