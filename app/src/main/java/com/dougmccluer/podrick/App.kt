package com.dougmccluer.podrick

import android.app.Application
import com.dougmccluer.podrick.di.podsmods
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.KoinApplication
import org.koin.core.context.startKoin

class App:Application() {

	private var koinApp:KoinApplication? = null

	override fun onCreate() {
		super.onCreate()
		koinApp ?: let {
			koinApp = startKoin {
				androidLogger()
				androidContext(this@App)
				modules(listOf(podsmods))
			}
		}
	}

}



