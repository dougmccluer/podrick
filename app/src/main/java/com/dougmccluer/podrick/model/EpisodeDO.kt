package com.dougmccluer.podrick.model

import java.time.ZonedDateTime

data class EpisodeDO (
	val title:String,
	val description:String,
	val publishedDate:ZonedDateTime,
	val length:Long,
	val progress:Long,
	val completed:Boolean
)
