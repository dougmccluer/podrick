package com.dougmccluer.podrick.model

data class SubscriptionDO(
	val title:String,
	val episodes:ArrayList<EpisodeDO>
)
