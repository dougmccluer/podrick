package com.dougmccluer.podrick

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.SavedStateHandle
import com.dougmccluer.podrick.event.observeEvent
import com.squareup.workflow1.ui.ViewRegistry
import com.squareup.workflow1.ui.WorkflowLayout
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

@OptIn(WorkflowUiExperimentalApi::class)
class MainActivity:AppCompatActivity() {

	private val viewRegistry:ViewRegistry by inject()

	override fun onCreate(savedInstanceState:Bundle?) {
		super.onCreate(savedInstanceState)

		val viewModel:MainActivityViewModel by viewModel{
			parametersOf( SavedStateHandle() )
		}

		setContentView(
			WorkflowLayout(this).apply {
				start(viewModel.renderings, viewRegistry)
			}
		)

		observeEvent(viewModel.backPressEvent){
			finish()
		}
	}
}

