package com.dougmccluer.podrick.util

fun firstNonBlank(vararg str:String?):String? = str.first { it?.isNotBlank()==true }
