package com.dougmccluer.podrick.util

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory

object PodrickSnapshotter:IStateSerializer {
	override val charset = Charsets.UTF_8

	override fun <T> serialize(state:T, type:Class<T>, labelKey:String):String? {
		return try {
			Moshi.Builder()
				.add(PolymorphicJsonAdapterFactory.of(type, labelKey))
				.build()
				.adapter(type)
				.nullSafe()
				.toJson(state)
		} catch (tr:Throwable) {
			Plog.log("failed to serialize state: ${type.canonicalName}", Plog.ERROR, false, tr)
			null
		}
	}

	override fun <T> deserialize(serializedState:String, outputType:Class<T>, labelKey:String):T? {
		return try {
			Moshi.Builder()
				.add(PolymorphicJsonAdapterFactory.of(outputType, labelKey))
				.build()
				.adapter(outputType)
				.nullSafe()
				.fromJson(serializedState)
		} catch (tr:Throwable) {
			Plog.log("failed to deserialize state: ${outputType.canonicalName}", Plog.ERROR, false, tr)
			null
		}
	}
}
