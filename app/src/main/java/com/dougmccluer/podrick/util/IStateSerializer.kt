package com.dougmccluer.podrick.util

import java.nio.charset.Charset

interface IStateSerializer {
	fun <T> serialize(state:T, type:Class<T>, labelKey:String="name"):String?
	fun <T> deserialize(serializedState:String, outputType:Class<T>, labelKey:String="name"):T?
	val charset:Charset
}
