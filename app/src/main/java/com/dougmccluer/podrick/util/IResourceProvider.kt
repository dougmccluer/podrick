package com.dougmccluer.podrick.util

import android.graphics.Typeface
import androidx.annotation.*
import java.io.InputStream

interface IResourceProvider {
	fun getString(@StringRes resId:Int):String

	fun getString(@StringRes resId:Int, formatArg:String?):String

	fun getColor(@ColorRes resId:Int):Int

	fun openRawResource(@RawRes resId:Int):InputStream

	fun getFont(@FontRes resId:Int):Typeface?

	fun getDimension(resId:Int):Float

	fun getResourceName(resId:Int?):String
}