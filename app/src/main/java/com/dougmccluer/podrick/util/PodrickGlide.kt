package com.dougmccluer.podrick.util

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class PodrickGlide:AppGlideModule() {
	override fun isManifestParsingEnabled():Boolean {
		return false
	}

}