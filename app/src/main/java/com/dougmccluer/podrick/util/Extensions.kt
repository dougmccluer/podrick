package com.dougmccluer.podrick.util

import android.view.View
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData

/** if this field is empty, then hide the TextView that is supposed to display it.  Otherwise,
 * show the TextView, and set its text
 * */
fun TextView.setTextOrHide(newText:String?, vararg alsoHide:View?) {
	if (newText?.isNotBlank() == true) {
		text = newText
		isVisible = true
	} else {
		isVisible = false
	}
}
