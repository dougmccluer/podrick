package com.dougmccluer.podrick.util

import android.content.Context
import android.graphics.Typeface
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import java.io.InputStream

class ResourceProvider(private val context:Context):IResourceProvider {

	override fun getString(@StringRes resId:Int):String = context.getString(resId)

	override fun getString(resId:Int, formatArg:String?):String =
		context.getString(resId, formatArg)

	override fun getColor(@ColorRes resId:Int):Int = ContextCompat.getColor(context, resId)

	override fun openRawResource(resId:Int):InputStream = context.resources.openRawResource(resId)

	override fun getFont(resId:Int):Typeface? = ResourcesCompat.getFont(context, resId)

	override fun getDimension(resId:Int):Float = context.resources.getDimension(resId)

	override fun getResourceName(resId:Int?):String = resId?.let{context.resources.getResourceName(resId)} ?: "null"
}