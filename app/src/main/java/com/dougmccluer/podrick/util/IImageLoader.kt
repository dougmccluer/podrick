package com.dougmccluer.podrick.util

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.amulyakhare.textdrawable.TextDrawable
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

interface IImageLoader {
	fun loadImage(
		url:String?,
		targetView:ImageView,
		placeHolder:Drawable? = null,
		onComplete:() -> Unit = {},
		onFailed:(tr:Throwable?) -> Unit = {},
		)

	fun preload(
		context:Context,
		url:String,
		onComplete:() -> Unit = {},
		onFailed:(tr:Throwable?) -> Unit = {}
	)

	fun preload(
		context:Context,
		url:String,
		width:Int,
		height:Int,
		onComplete:() -> Unit = {},
		onFailed:(tr:Throwable?) -> Unit = {}
	)

	companion object {

		fun makeTextPlaceholder(
			text:String?,
			radius:Int = 20,
			colors:Array<Int>? = null,
			textPxSize:Int = 80,
			textColor:Int = Color.WHITE,
			font:Typeface = Typeface.SANS_SERIF
		):Drawable {

			var initials:String? = null
			text?.let {
				val words = it.split(" ")
				if (words.size > 1 && words[0].isNotBlank() && words[1].isNotBlank()) {
					initials = "${words[0].first()}${words[1].first()}"
				} else if (text.length > 1) {
					initials = it.substring(0, 2)
				}
			}

			if (initials == null) {
				initials = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray().let {
					"${it.random()}${it.random()}"
				}
			}

			val bgColor:Int = colors?.random() ?: arrayOf(
				0xFFC62828,
				0xFFAD1457,
				0xFF6A1B9A,
				0xFF4527A0,
				0xFF283593,
				0xFF0277BD,
				0xFF1565C0,
				0xFF00838F,
				0xFF00695C
			).random().toInt()

			return TextDrawable.builder().beginConfig()
				.textColor(textColor)
				.useFont(font)
				.fontSize(textPxSize)
				.endConfig()
				.buildRoundRect(initials, bgColor, radius)
		}

		fun getDefaultImplementation():IImageLoader {
			return object:IImageLoader {
				override fun loadImage(
					url:String?,
					targetView:ImageView,
					placeHolder:Drawable?,
					onComplete:() -> Unit,
					onFailed:(tr:Throwable?) -> Unit,
				) {
					val listener = object:RequestListener<Drawable> {
						override fun onLoadFailed(
							e:GlideException?,
							model:Any?,
							target:Target<Drawable>?,
							isFirstResource:Boolean
						):Boolean {
							onFailed(e)
							return false
						}

						override fun onResourceReady(
							resource:Drawable?,
							model:Any?,
							target:Target<Drawable>?,
							dataSource:DataSource?,
							isFirstResource:Boolean
						):Boolean {
							onComplete()
							return false
						}
					}

					GlideApp.with(targetView.context)
						.load(url)
						.placeholder(placeHolder)
						.transition(DrawableTransitionOptions.withCrossFade())
						.addListener(listener)
						.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
						.into(targetView)
						.clearOnDetach()
				}

				override fun preload(
					context:Context,
					url:String,
					onComplete:() -> Unit,
					onFailed:(tr:Throwable?) -> Unit
				) {
					GlideApp.with(context)
						.load(url)
						.preload()
				}

				override fun preload(
					context:Context,
					url:String,
					width:Int,
					height:Int,
					onComplete:() -> Unit,
					onFailed:(tr:Throwable?) -> Unit
				) {
					GlideApp.with(context)
						.load(url)
						.preload(width, height)
				}
			}
		}
	}
}