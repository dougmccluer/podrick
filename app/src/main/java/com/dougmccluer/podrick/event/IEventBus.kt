package com.dougmccluer.podrick.event

import io.reactivex.Observable

interface IEventBus {
	fun postEvent(event:Any)
	fun observeEvents():Observable<Any>
	fun observeEventsOnUi():Observable<Any>
}

inline fun <reified T:Any> IEventBus.observeEvent():Observable<T> {
	return observeEvents().ofType(T::class.java)
}

inline fun <reified T:Any> IEventBus.observeEventOnUi():Observable<T> {
	return observeEventsOnUi().ofType(T::class.java)
}