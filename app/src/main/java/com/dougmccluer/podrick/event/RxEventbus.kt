package com.dougmccluer.podrick.event

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject

class RxBus:IEventBus {
	private val publishProcessor = PublishSubject.create<Any>()

	override fun postEvent(event:Any) {
		publishProcessor.onNext(event)
	}

	override fun observeEvents():Observable<Any> {
		return publishProcessor.serialize()
	}

	override fun observeEventsOnUi():Observable<Any> {
		return observeEvents()
			.observeOn(AndroidSchedulers.mainThread())
	}
}