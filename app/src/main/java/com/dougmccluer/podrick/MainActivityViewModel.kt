package com.dougmccluer.podrick

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dougmccluer.podrick.event.EventDispatcher
import com.dougmccluer.podrick.event.EventStream
import com.dougmccluer.podrick.event.dispatch
import com.dougmccluer.podrick.screens.shell.ShellOutput
import com.dougmccluer.podrick.screens.shell.ShellRendering
import com.dougmccluer.podrick.screens.shell.ShellWorkflow
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import com.squareup.workflow1.ui.renderWorkflowIn
import kotlinx.coroutines.flow.Flow

class MainActivityViewModel(
	savedState:SavedStateHandle,
	workflow:ShellWorkflow
):ViewModel() {

	private val _backPressEvent = EventDispatcher<Unit>()
	val backPressEvent:EventStream<Unit> = _backPressEvent

	@OptIn(WorkflowUiExperimentalApi::class)
	val renderings:Flow<ShellRendering> = renderWorkflowIn(
		workflow = workflow,
		scope = viewModelScope,
		savedStateHandle = savedState,
		interceptors = listOf(
			//SimpleLoggingWorkflowInterceptor()
		)
	) {
		handleWorkflowOutput(it)
	}

	private fun handleWorkflowOutput(output:ShellOutput) {
		when (output) {
			ShellOutput.Back -> {
				//Workflow has declined to handle the back press. Forward the event to the Activity
				_backPressEvent.dispatch()
			}
		}
	}

}