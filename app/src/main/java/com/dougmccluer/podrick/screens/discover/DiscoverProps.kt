package com.dougmccluer.podrick.screens.discover

import de.mfietz.fyydlin.SearchHit

data class DiscoverProps(
	val query:String?=null,
	val results:List<SearchHit>? = null
)
