package com.dougmccluer.podrick.screens.detail

import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.view.isVisible
import com.dougmccluer.podrick.R
import com.dougmccluer.podrick.databinding.DetailScreenBinding
import com.dougmccluer.podrick.util.IImageLoader
import com.dougmccluer.podrick.util.firstNonBlank
import com.dougmccluer.podrick.util.setTextOrHide
import com.squareup.workflow1.ui.ViewEnvironment
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import java.text.SimpleDateFormat

@OptIn(WorkflowUiExperimentalApi::class)
class DetailView(
	context:Context,
	val imageLoader:IImageLoader
):FrameLayout(context) {
	private val binding = DetailScreenBinding.inflate(LayoutInflater.from(context), this, true)

	fun showRendering(rendering:DetailRendering, env:ViewEnvironment) {
		binding.apply {
			rendering.podcastInfo.let {
				detailTitle.setTextOrHide(newText = it.title)
				detailSubtitle.setTextOrHide(newText = it.subtitle)
				detailDescription.setTextOrHide(newText = it.description)

				//some podcasts duplicate the description in the subtitle, or duplicate the title in
				//the subtitle.  If that's the case here, hide the subtitle
				binding.detailSubtitle.isVisible = it.subtitle != it.description && it.subtitle != it.title

				val placeholder = IImageLoader.makeTextPlaceholder(
				text = it.title,
				radius = context.resources.getDimensionPixelSize(R.dimen.label_margin),
				textPxSize = (context.resources.getDimensionPixelSize(R.dimen.thumbnail_size) * .5).toInt()
			)

				imageLoader.loadImage(
					url = firstNonBlank(
						it.imageUrl,
						it.layoutImageUrl,
						it.smallImageURL
					),
					targetView = detailImage,
					placeHolder = placeholder
				)

				rssUrlDisplay.setTextOrHide(it.xmlUrl, rssLabel)
				authorDisplay.setTextOrHide(it.author, authorLabel)

				if (it.lastPubDate != null) {
					lastPublishedDisplay.text =
						SimpleDateFormat.getInstance().format(it.lastPubDate)
				} else {
					lastPublishedDisplay.isVisible = false
					lastPublishedLabel.isVisible = false
				}

				episodeCountDisplay.setTextOrHide(it.countEpisodes.toString(), episodeCountLabel)

			}
		}
	}
}