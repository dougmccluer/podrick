package com.dougmccluer.podrick.screens.welcome

import android.animation.Animator
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewPropertyAnimator
import android.widget.FrameLayout
import androidx.transition.Transition
import com.dougmccluer.podrick.databinding.WelcomeScreenBinding
import com.squareup.workflow1.ui.ViewEnvironment
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi

@OptIn(WorkflowUiExperimentalApi::class)
class WelcomeView(
	context:Context
):FrameLayout(context), Transition.TransitionListener {

	private val binding = WelcomeScreenBinding.inflate(LayoutInflater.from(context), this, true)
	private var animator:ViewPropertyAnimator?=null

	fun showRendering(rendering:WelcomeRendering, env:ViewEnvironment) {
		binding.apply {
			if(rendering.doAnim) {
				animator = startAnimation()
			}

			subscriptionsBtn.setOnClickListener {
				rendering.onSubscriptionsClicked()
			}
		}
	}

	fun startAnimation():ViewPropertyAnimator? {
		binding.welcomeMessage.alpha = 0F
		binding.welcomeMessage.translationY = 200F  //TODO:use a value relative to screen size
		return binding.welcomeMessage.animate().apply {
			alpha(1F)
			translationY(0F)
			setListener(animationListener)
			duration = resources.getInteger(android.R.integer.config_mediumAnimTime).toLong()
		}
	}

	private val animationListener = object:Animator.AnimatorListener{
		override fun onAnimationStart(animation:Animator?) {}

		override fun onAnimationEnd(animation:Animator?) {
			animation?.removeListener(this)
			animator = null
		}

		override fun onAnimationCancel(animation:Animator?) {
			animation?.removeListener(this)
			animator = null
		}

		override fun onAnimationRepeat(animation:Animator?) {}
	}

	//
	override fun onTransitionStart(transition:Transition) {
		animator?.cancel()
	}

	override fun onTransitionEnd(transition:Transition) {
		transition.removeListener(this)
		startAnimation()
	}

	override fun onTransitionCancel(transition:Transition) {}
	override fun onTransitionPause(transition:Transition) {}
	override fun onTransitionResume(transition:Transition) {}
}