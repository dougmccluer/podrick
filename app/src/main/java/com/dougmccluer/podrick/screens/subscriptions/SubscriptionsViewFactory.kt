package com.dougmccluer.podrick.screens.subscriptions

import android.content.Context
import android.view.ViewGroup
import com.squareup.workflow1.ui.ViewEnvironment
import com.squareup.workflow1.ui.ViewFactory
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import com.squareup.workflow1.ui.bindShowRendering

//TODO: add this to the viewRegistry!

@OptIn(WorkflowUiExperimentalApi::class)
class SubscriptionsViewFactory(
):ViewFactory<SubscriptionsRendering> {

	override val type = SubscriptionsRendering::class

	override fun buildView(
		initialRendering:SubscriptionsRendering,
		initialViewEnvironment:ViewEnvironment,
		contextForNewView:Context,
		container:ViewGroup?
	) = SubscriptionsView(context = contextForNewView)
		.apply {
			bindShowRendering(
				initialRendering,
				initialViewEnvironment
			) { rendering:SubscriptionsRendering, environment:ViewEnvironment ->
				showRendering(rendering, environment)
			}
		}
}