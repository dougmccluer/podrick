package com.dougmccluer.podrick.screens.shell

import com.dougmccluer.podrick.event.IEventBus
import com.dougmccluer.podrick.screens.PodrickScreen
import com.dougmccluer.podrick.views.toolbar.ToolbarRendering
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import com.squareup.workflow1.ui.backstack.BackStackScreen

@OptIn(WorkflowUiExperimentalApi::class)
data class ShellRendering  constructor(
	val screenRendering:BackStackScreen<PodrickScreen>,
	val toolbar:ToolbarRendering,
	val onBack:()->Unit,
	private val eventBus:IEventBus
)