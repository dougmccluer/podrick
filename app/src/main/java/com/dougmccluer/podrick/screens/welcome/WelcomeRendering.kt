package com.dougmccluer.podrick.screens.welcome

import com.dougmccluer.podrick.screens.PodrickScreen
import com.dougmccluer.podrick.util.IResourceProvider

data class WelcomeRendering(
	override val resourceProvider:IResourceProvider,
	val doAnim:Boolean=false,
	val onSubscriptionsClicked:()->Unit
	):PodrickScreen{

	override val screenTitle:String = "Welcome"
}
