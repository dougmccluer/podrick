package com.dougmccluer.podrick.screens

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.transition.*
import com.dougmccluer.podrick.R
import com.squareup.workflow1.ui.BuilderViewFactory
import com.squareup.workflow1.ui.ViewFactory
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import com.squareup.workflow1.ui.backstack.BackStackContainer
import com.squareup.workflow1.ui.backstack.BackStackScreen
import com.squareup.workflow1.ui.bindShowRendering

@OptIn(WorkflowUiExperimentalApi::class)
class PodrickBackStackContainer @JvmOverloads constructor(
	context:Context,
	attributeSet:AttributeSet? = null,
	defStyle:Int = 0,
	defStyleRes:Int = 0
):
	BackStackContainer(context, attributeSet, defStyle, defStyleRes) {
	override fun performTransition(oldViewMaybe:View?, newView:View, popped:Boolean) {
		// Showing something already, transition with push or pop effect.
		oldViewMaybe
			?.let { oldView ->
				val oldBody:View? = oldView.findViewById(R.id.back_stack_body)
				val newBody:View? = newView.findViewById(R.id.back_stack_body)

				val oldTarget:View
				val newTarget:View
				if (oldBody != null && newBody != null) {
					oldTarget = oldBody
					newTarget = newBody
				} else {
					oldTarget = oldView
					newTarget = newView
				}

				val (outEdge, inEdge) = when (popped) {
					false -> Gravity.START to Gravity.END
					true -> Gravity.END to Gravity.START
				}

				val transition = TransitionSet()
					.addTransition(Slide(outEdge).addTarget(oldTarget))
					.addTransition(Slide(inEdge).addTarget(newTarget))
					.setInterpolator(AccelerateDecelerateInterpolator())

				if (newView is Transition.TransitionListener) {
					transition.addListener(newView)
				}

				TransitionManager.go(Scene(this, newView), transition)
				return
			}

		// This is the first view, just show it.
		addView(newView)
	}

	public companion object : ViewFactory<BackStackScreen<*>>
	by BuilderViewFactory(
		type = BackStackScreen::class,
		viewConstructor = { initialRendering, initialEnv, context, _ ->
			PodrickBackStackContainer(context)
				.apply {
					id = R.id.workflow_back_stack_container
					layoutParams = (ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
					bindShowRendering(initialRendering, initialEnv, ::update)
				}
		}
	)
}