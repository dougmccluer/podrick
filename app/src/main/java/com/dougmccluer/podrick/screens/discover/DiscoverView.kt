package com.dougmccluer.podrick.screens.discover

import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.dougmccluer.podrick.databinding.DiscoverScreenBinding
import com.dougmccluer.podrick.util.IImageLoader
import com.dougmccluer.podrick.views.SearchResultsRecyclerAdapter
import com.squareup.workflow1.ui.ViewEnvironment
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi


@OptIn(WorkflowUiExperimentalApi::class)
class DiscoverView(
	val imageLoader:IImageLoader,
	context:Context
):FrameLayout(context) {

	private val binding = DiscoverScreenBinding.inflate(LayoutInflater.from(context), this, true)

	fun showRendering(rendering:DiscoverRendering, env:ViewEnvironment) {
		binding.apply {
			if (rendering.results?.isNotEmpty() == true) {
				searchResultsDisplay.isVisible = true
				searchResultsDisplay.layoutManager = LinearLayoutManager(
					context,
					LinearLayoutManager.VERTICAL,
					false
				)

				SearchResultsRecyclerAdapter(imageLoader, rendering.onItemSelected).let {
					searchResultsDisplay.adapter = it
					it.setData(rendering.results)
				}
			} else {
				searchResultsDisplay.isVisible = false
			}
		}
	}

}