package com.dougmccluer.podrick.screens.discover

import android.content.Context
import android.view.ViewGroup
import com.dougmccluer.podrick.util.IImageLoader
import com.squareup.workflow1.ui.ViewEnvironment
import com.squareup.workflow1.ui.ViewFactory
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import com.squareup.workflow1.ui.bindShowRendering

@OptIn(WorkflowUiExperimentalApi::class)
class DiscoverViewFactory(
	val imageLoader:IImageLoader
):ViewFactory<DiscoverRendering> {

	override val type = DiscoverRendering::class

	override fun buildView(
		initialRendering:DiscoverRendering,
		initialViewEnvironment:ViewEnvironment,
		contextForNewView:Context,
		container:ViewGroup?
	) = DiscoverView(imageLoader = imageLoader, context = contextForNewView)
		.apply {
			bindShowRendering(
				initialRendering,
				initialViewEnvironment
			) { rendering:DiscoverRendering, environment:ViewEnvironment ->
				showRendering(rendering, environment)
			}
		}
}
