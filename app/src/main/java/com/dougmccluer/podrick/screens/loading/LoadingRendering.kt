package com.dougmccluer.podrick.screens.loading

import com.dougmccluer.podrick.R
import com.dougmccluer.podrick.screens.PodrickScreen
import com.dougmccluer.podrick.util.IResourceProvider

data class LoadingRendering(
	override val resourceProvider:IResourceProvider,
):PodrickScreen {
	override val screenTitle = resourceProvider.getString(R.string.discover_screen)
}