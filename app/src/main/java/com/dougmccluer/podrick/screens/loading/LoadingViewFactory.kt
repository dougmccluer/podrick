package com.dougmccluer.podrick.screens.loading

import com.dougmccluer.podrick.databinding.LoadingScreenBinding
import com.squareup.workflow1.ui.LayoutRunner
import com.squareup.workflow1.ui.ViewEnvironment
import com.squareup.workflow1.ui.ViewFactory
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi

@OptIn(WorkflowUiExperimentalApi::class)
internal val LoadingViewFactory:ViewFactory<LoadingRendering> =
	LayoutRunner.bind(LoadingScreenBinding::inflate) { data:LoadingRendering,
													   env:ViewEnvironment ->

	}