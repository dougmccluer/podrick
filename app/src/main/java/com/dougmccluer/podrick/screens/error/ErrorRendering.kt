package com.dougmccluer.podrick.screens.error

import com.dougmccluer.podrick.screens.PodrickScreen
import com.dougmccluer.podrick.util.IResourceProvider

data class ErrorRendering(
	val errorMessage:String,
	override val resourceProvider:IResourceProvider
	):PodrickScreen{
	override val screenTitle="Error"
}
