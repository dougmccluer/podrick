package com.dougmccluer.podrick.screens.subscriptions

import com.dougmccluer.podrick.R
import com.dougmccluer.podrick.screens.PodrickScreen
import com.dougmccluer.podrick.util.IResourceProvider
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi

@OptIn(WorkflowUiExperimentalApi::class)
class SubscriptionsRendering(override val resourceProvider:IResourceProvider):PodrickScreen {
	override val screenTitle:String = resourceProvider.getString(R.string.subscriptions_screen_title)
}