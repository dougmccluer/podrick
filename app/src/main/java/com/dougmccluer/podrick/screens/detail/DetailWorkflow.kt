package com.dougmccluer.podrick.screens.detail

import com.dougmccluer.podrick.screens.detail.DetailWorkflow.State
import com.dougmccluer.podrick.util.IResourceProvider
import com.dougmccluer.podrick.util.IStateSerializer
import com.squareup.workflow1.Snapshot
import com.squareup.workflow1.StatefulWorkflow
import de.mfietz.fyydlin.SearchHit

class DetailWorkflow(
	private val resourceProvider:IResourceProvider,
	private val snapshotter:IStateSerializer
):StatefulWorkflow<DetailWorkflow.DetailProps, State, DetailOutput, DetailRendering>() {

	sealed class State {
		data class Default(val podcastInfo:SearchHit):State()
	}

	data class DetailProps(val podcastInfo:SearchHit)

	override fun initialState(props:DetailProps, snapshot:Snapshot?):State {
		return State.Default(props.podcastInfo)
	}

	override fun onPropsChanged(old:DetailProps, new:DetailProps, state:State):State {
		return State.Default(new.podcastInfo)
	}

	override fun render(props:DetailProps, state:State, context:RenderContext):DetailRendering {
		return when (state) {
			is State.Default -> {
				DetailRendering(
					resourceProvider = resourceProvider,
					podcastInfo = state.podcastInfo
				)
			}
		}
	}

	override fun snapshotState(state:State):Snapshot = Snapshot.write { sink ->
		snapshotter.serialize(state, State::class.java)?.let {
			sink.writeString(it, snapshotter.charset)
		}
	}
}

