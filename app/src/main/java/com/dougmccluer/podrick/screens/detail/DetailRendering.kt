package com.dougmccluer.podrick.screens.detail

import com.dougmccluer.podrick.R
import com.dougmccluer.podrick.screens.PodrickScreen
import com.dougmccluer.podrick.util.IResourceProvider
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import de.mfietz.fyydlin.SearchHit

@OptIn(WorkflowUiExperimentalApi::class)
data class DetailRendering(
	override val resourceProvider:IResourceProvider,
	val podcastInfo:SearchHit
):PodrickScreen {
	override val screenTitle = resourceProvider.getString(R.string.detail_screen_title)
}