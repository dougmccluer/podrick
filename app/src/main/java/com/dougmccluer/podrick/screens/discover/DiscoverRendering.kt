package com.dougmccluer.podrick.screens.discover

import com.dougmccluer.podrick.R
import com.dougmccluer.podrick.event.IEventBus
import com.dougmccluer.podrick.screens.PodrickScreen
import com.dougmccluer.podrick.util.IResourceProvider
import de.mfietz.fyydlin.SearchHit

data class DiscoverRendering(
	override val resourceProvider:IResourceProvider,
	val onItemSelected:(SearchHit) -> Unit,
	val results:List<SearchHit>? = null,
	val onSearchClicked:(query:String) -> Unit,
	val eventBus:IEventBus
):PodrickScreen {
	override val screenTitle = resourceProvider.getString(R.string.discover_screen)
}