package com.dougmccluer.podrick.screens.welcome

import android.content.Context
import android.view.ViewGroup
import com.squareup.workflow1.ui.ViewEnvironment
import com.squareup.workflow1.ui.ViewFactory
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import com.squareup.workflow1.ui.bindShowRendering

//TODO: add this to the viewRegistry!

@OptIn(WorkflowUiExperimentalApi::class)
class WelcomeViewFactory(
):ViewFactory<WelcomeRendering> {

	override val type = WelcomeRendering::class

	override fun buildView(
		initialRendering:WelcomeRendering,
		initialViewEnvironment:ViewEnvironment,
		contextForNewView:Context,
		container:ViewGroup?
	) = WelcomeView(context = contextForNewView)
		.apply {
			bindShowRendering(
				initialRendering,
				initialViewEnvironment
			) { rendering:WelcomeRendering, environment:ViewEnvironment ->
				showRendering(rendering, environment)
			}
		}
}