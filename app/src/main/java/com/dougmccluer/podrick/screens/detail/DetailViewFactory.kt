package com.dougmccluer.podrick.screens.detail

import android.content.Context
import android.view.ViewGroup
import com.dougmccluer.podrick.util.IImageLoader
import com.squareup.workflow1.ui.ViewEnvironment
import com.squareup.workflow1.ui.ViewFactory
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import com.squareup.workflow1.ui.bindShowRendering

@OptIn(WorkflowUiExperimentalApi::class)
class DetailViewFactory(
	private val imageLoader:IImageLoader
):ViewFactory<DetailRendering> {

	override val type = DetailRendering::class

	override fun buildView(
		initialRendering:DetailRendering,
		initialViewEnvironment:ViewEnvironment,
		contextForNewView:Context,
		container:ViewGroup?
	) = DetailView(context = contextForNewView, imageLoader = imageLoader)
		.apply {
			bindShowRendering(
				initialRendering,
				initialViewEnvironment
			) { rendering:DetailRendering, environment:ViewEnvironment ->
				showRendering(rendering, environment)
			}
		}
}