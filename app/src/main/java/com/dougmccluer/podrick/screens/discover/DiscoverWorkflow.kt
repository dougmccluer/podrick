package com.dougmccluer.podrick.screens.discover

import com.dougmccluer.podrick.event.IEventBus
import com.dougmccluer.podrick.screens.PodrickScreen
import com.dougmccluer.podrick.screens.discover.DiscoverWorkflow.State
import com.dougmccluer.podrick.screens.error.ErrorRendering
import com.dougmccluer.podrick.screens.loading.LoadingRendering
import com.dougmccluer.podrick.screens.shell.ShellWorkflow
import com.dougmccluer.podrick.util.IResourceProvider
import com.dougmccluer.podrick.util.IStateSerializer
import com.dougmccluer.podrick.util.Plog
import com.squareup.workflow1.Snapshot
import com.squareup.workflow1.StatefulWorkflow
import com.squareup.workflow1.action
import com.squareup.workflow1.runningWorker
import com.squareup.workflow1.rx2.asWorker
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import de.mfietz.fyydlin.FyydClient
import de.mfietz.fyydlin.FyydResponse
import de.mfietz.fyydlin.SearchHit
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

@OptIn(WorkflowUiExperimentalApi::class)
class DiscoverWorkflow(
	private val resourceProvider:IResourceProvider,
	private val fyydClient:FyydClient,
	private val eventBus:IEventBus,
	private val snapshotter:IStateSerializer
):StatefulWorkflow<DiscoverProps, State, DiscoverOutput, PodrickScreen>() {

	sealed class State(val name:String) {
		data class Search(val results:List<SearchHit>? = null):State(name = "Search")
		data class Loading(val searchString:String):State("Loading")
		data class Error(val searchError:Throwable):State("Error")
	}

	override fun initialState(props:DiscoverProps, snapshot:Snapshot?):State {
		return when{
			props.query?.isNotEmpty()==true -> State.Loading(props.query)
			else->State.Search()
		}
	}

	override fun onPropsChanged(old:DiscoverProps, new:DiscoverProps, state:State):State {
		if( new.query!= old.query ){
			return when (new.query){
				null -> State.Search()
				else -> State.Loading(new.query)
			}
		}
		return super.onPropsChanged(old, new, state)
	}

	override fun render(props:DiscoverProps, state:State, context:RenderContext):PodrickScreen {
		return when (state) {
			is State.Search -> {
				DiscoverRendering(
					resourceProvider = resourceProvider,
					results = state.results,
					onItemSelected = { podcastInfo:SearchHit ->
						Plog.log("selected item: $podcastInfo")
						context.actionSink.send(action {
							setOutput(DiscoverOutput(podcastInfo))
						})
					},
					onSearchClicked = context.eventHandler { searchString ->
						this.state = State.Loading(searchString)
					},
					eventBus = eventBus,
				)
			}

			is State.Loading -> {
				context.runningWorker(
					fyydClient.searchPodcasts(state.searchString)
						.subscribeOn(Schedulers.io())
						.observeOn(AndroidSchedulers.mainThread())
						.doOnError { error ->
							Plog.log("search failed", Plog.ERROR, false, error)
							context.actionSink.send(action { this.state = State.Error(error) })
						}
						.asWorker()
				) { response:FyydResponse ->
					Plog.log(response.data.toString())
					action { this.state = State.Search(results = response.data) }
				}

				LoadingRendering(
					resourceProvider = resourceProvider,
				)
			}

			is State.Error ->{
				val msg = when(state.searchError){
					is HttpException -> "connection error"
					else-> "unknown error"
				}
				ErrorRendering( msg, resourceProvider)
			}
		}
	}

	override fun snapshotState(state:State):Snapshot = Snapshot.write { sink->
		snapshotter.serialize(state, State::class.java)?.let{
			sink.writeString( it, snapshotter.charset )
		}
	}
}