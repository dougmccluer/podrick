package com.dougmccluer.podrick.screens

import com.dougmccluer.podrick.util.IResourceProvider

interface PodrickScreen {
	val screenTitle:String
	val resourceProvider:IResourceProvider
}