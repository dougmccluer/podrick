package com.dougmccluer.podrick.screens.discover

import de.mfietz.fyydlin.SearchHit

data class DiscoverOutput(val searchHit:SearchHit)
