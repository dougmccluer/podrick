package com.dougmccluer.podrick.screens.error

import android.content.Context
import android.view.ViewGroup
import com.squareup.workflow1.ui.ViewEnvironment
import com.squareup.workflow1.ui.ViewFactory
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import com.squareup.workflow1.ui.bindShowRendering

@OptIn(WorkflowUiExperimentalApi::class)
class ErrorViewFactory(
):ViewFactory<ErrorRendering> {

	override val type = ErrorRendering::class

	override fun buildView(
		initialRendering:ErrorRendering,
		initialViewEnvironment:ViewEnvironment,
		contextForNewView:Context,
		container:ViewGroup?
	) = ErrorView(context = contextForNewView)
		.apply {
			bindShowRendering(
				initialRendering,
				initialViewEnvironment
			) { rendering:ErrorRendering, environment:ViewEnvironment ->
				showRendering(rendering, environment)
			}
		}
}