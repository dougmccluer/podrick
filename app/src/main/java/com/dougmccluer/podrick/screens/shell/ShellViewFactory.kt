package com.dougmccluer.podrick.screens.shell

import android.content.Context
import android.view.ViewGroup
import com.squareup.workflow1.ui.*

@OptIn(WorkflowUiExperimentalApi::class)
class ShellViewFactory(
):ViewFactory<ShellRendering> {

	override val type = ShellRendering::class

	override fun buildView(
		initialRendering:ShellRendering,
		initialViewEnvironment:ViewEnvironment,
		contextForNewView:Context,
		container:ViewGroup?
	) = ShellView(context = contextForNewView)
		.apply {
			bindShowRendering(
				initialRendering,
				initialViewEnvironment
			) { rendering:ShellRendering, environment:ViewEnvironment ->
				showRendering(rendering, environment)
			}
		}
}