package com.dougmccluer.podrick.screens.shell

import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.dougmccluer.podrick.databinding.ShellScreenBinding
import com.dougmccluer.podrick.screens.PodrickScreen
import com.dougmccluer.podrick.views.toolbar.ToolbarRendering
import com.squareup.workflow1.ui.ViewEnvironment
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import com.squareup.workflow1.ui.backPressedHandler
import com.squareup.workflow1.ui.backstack.BackStackScreen

@OptIn(WorkflowUiExperimentalApi::class)
class ShellView(
	context:Context,
):FrameLayout(context) {

	private val binding = ShellScreenBinding.inflate(LayoutInflater.from(context), this, true)

	private var toolbarRendering:ToolbarRendering?=null
	private var screenRendering:BackStackScreen<PodrickScreen>?=null

	fun showRendering(rendering:ShellRendering, env:ViewEnvironment) {
		binding.apply {
			if(rendering.toolbar!= null
				&& rendering.toolbar != toolbarRendering){
				toolbarRendering = rendering.toolbar
				toolbarContainer.update(rendering.toolbar, env)
			}

			if(rendering.screenRendering!=null
				&& rendering.screenRendering != screenRendering){
				screenRendering = rendering.screenRendering
				mainContainer.update(rendering.screenRendering, env)
			}

			root.backPressedHandler = {
				rendering.onBack()
			}
		}
	}
}