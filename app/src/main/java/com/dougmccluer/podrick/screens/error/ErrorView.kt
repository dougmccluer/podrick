package com.dougmccluer.podrick.screens.error

import android.content.Context
import android.view.LayoutInflater
import android.widget.FrameLayout
import com.dougmccluer.podrick.databinding.ErrorScreenBinding
import com.squareup.workflow1.ui.ViewEnvironment
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi

@OptIn(WorkflowUiExperimentalApi::class)
class ErrorView(
	context:Context
):FrameLayout(context) {

	private val binding = ErrorScreenBinding.inflate(LayoutInflater.from(context), this, true)

	fun showRendering(rendering:ErrorRendering, env:ViewEnvironment) {
		binding
	}
}