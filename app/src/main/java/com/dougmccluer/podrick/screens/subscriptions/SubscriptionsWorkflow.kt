package com.dougmccluer.podrick.screens.subscriptions

import com.dougmccluer.podrick.model.EpisodeDO
import com.dougmccluer.podrick.model.SubscriptionDO
import com.dougmccluer.podrick.screens.subscriptions.SubscriptionsWorkflow.State
import com.dougmccluer.podrick.util.IResourceProvider
import com.dougmccluer.podrick.util.IStateSerializer
import com.dougmccluer.podrick.util.Plog
import com.squareup.workflow1.Snapshot
import com.squareup.workflow1.StatefulWorkflow
import com.squareup.workflow1.action
import com.squareup.workflow1.runningWorker
import com.squareup.workflow1.rx2.asWorker
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.time.ZonedDateTime

class SubscriptionsWorkflow(
	val resourceProvider:IResourceProvider,
	val snapshotter:IStateSerializer
):StatefulWorkflow<Unit, State, SubscriptionsOutput, SubscriptionsRendering>() {

	sealed class State {
		data class Default(
			val subscriptions:List<SubscriptionDO>? = null
		):State()

		object Loading:State()
	}

	override fun initialState(props:Unit, snapshot:Snapshot?):State {
		return State.Default()
	}

	override fun render(props:Unit, state:State, context:RenderContext):SubscriptionsRendering {
		return when (state) {
			is State.Loading -> {
				context.runningWorker(
					Single.just(generateTestSubscriptions())
						.subscribeOn(Schedulers.io())
						.observeOn(AndroidSchedulers.mainThread())
						.asWorker()
				) {
					action { this.state = SubscriptionsWorkflow.State.Default() }
				}

				SubscriptionsRendering(resourceProvider = resourceProvider)
			}

			is State.Default -> {

				context.runningSideEffect("fetchRss") {
					Plog.log("fetchRss")
				}

				SubscriptionsRendering(resourceProvider = resourceProvider)
			}
		}
	}

	override fun snapshotState(state:State):Snapshot = Snapshot.write { sink ->
		snapshotter.serialize(state, State::class.java)?.let {
			sink.writeString(it, snapshotter.charset)
		}
	}

	private fun generateTestSubscriptions():ArrayList<SubscriptionDO> {
		return arrayListOf(
			SubscriptionDO(
				title = "Test & Code",
				episodes = arrayListOf(
					EpisodeDO(
						title = "Episode 1",
						description = "First Episode!!",
						publishedDate = ZonedDateTime.now(),
						length = 1000L,
						progress = 50L,
						completed = false
					)
				)
			)
		)
	}
}


data class SubscriptionsOutput(val output:Any)
