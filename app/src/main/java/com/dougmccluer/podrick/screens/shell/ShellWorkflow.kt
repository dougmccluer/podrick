package com.dougmccluer.podrick.screens.shell

import com.dougmccluer.podrick.event.IEventBus
import com.dougmccluer.podrick.screens.PodrickScreen
import com.dougmccluer.podrick.screens.detail.DetailOutput
import com.dougmccluer.podrick.screens.detail.DetailWorkflow
import com.dougmccluer.podrick.screens.discover.DiscoverOutput
import com.dougmccluer.podrick.screens.discover.DiscoverProps
import com.dougmccluer.podrick.screens.discover.DiscoverWorkflow
import com.dougmccluer.podrick.screens.subscriptions.SubscriptionsOutput
import com.dougmccluer.podrick.screens.subscriptions.SubscriptionsWorkflow
import com.dougmccluer.podrick.screens.welcome.WelcomeRendering
import com.dougmccluer.podrick.util.IResourceProvider
import com.dougmccluer.podrick.util.IStateSerializer
import com.dougmccluer.podrick.util.Plog
import com.dougmccluer.podrick.views.toolbar.ToolbarWorkflow
import com.squareup.workflow1.*
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import com.squareup.workflow1.ui.backstack.toBackStackScreen
import de.mfietz.fyydlin.SearchHit

@OptIn(WorkflowUiExperimentalApi::class)
class ShellWorkflow(
	private val discoverWorkflow:DiscoverWorkflow,
	private val toolbarWorkflow:ToolbarWorkflow,
	private val detailWorkflow:DetailWorkflow,
	private val subscriptionsWorkflow:SubscriptionsWorkflow,
	private val resourceProvider:IResourceProvider,
	private val eventBus:IEventBus,
	private val snapshotter:IStateSerializer,
):StatefulWorkflow<
	Unit,
	ShellWorkflow.State,
	ShellOutput,
	ShellRendering
	>() {

	override fun render(
		props:Unit,
		state:State,
		context:RenderContext
	):ShellRendering {

		var renderingStack = mutableListOf<PodrickScreen>() + buildPodrickScreen(state, context)

		return ShellRendering(
			screenRendering = renderingStack.toBackStackScreen(),
			toolbar = context.renderChild(child = toolbarWorkflow) {
				handleToolbarOutput(it)
			},
			onBack = { context.actionSink.send(onBack()) },
			eventBus = eventBus
		)
	}

	fun onBack() = action {
		state.previousState?.let {
			this.state = it
		} ?: setOutput(ShellOutput.Back)
	}

	fun buildPodrickScreen(state:State, context:RenderContext):PodrickScreen = when (state) {
		is State.Welcome -> WelcomeRendering(
			resourceProvider = resourceProvider,
			doAnim = state.previousState == null,
			onSubscriptionsClicked = context.eventHandler {
				this.state = State.Subscriptions(state)
			}
		)

		is State.Discover -> {
			context.renderChild(
				child = discoverWorkflow,
				props = DiscoverProps(state.query)
			) {
				handleDiscoverOutput(it)
			}
		}
		is State.Detail -> {
			context.renderChild(
				child = detailWorkflow,
				props = DetailWorkflow.DetailProps(state.podcastInfo)
			) {
				handleDetailOutput(it)
			}
		}
		is State.Subscriptions -> {
			context.renderChild(
				child = subscriptionsWorkflow,
				props = Unit
			) {
				handleSubscriptionsOutput(it)
			}
		}
	}

	private fun handleSubscriptionsOutput(output:SubscriptionsOutput) = action {

	}

	private fun handleDetailOutput(output:DetailOutput) = action {
		TODO("Not yet implemented")
	}

	private fun handleDiscoverOutput(output:DiscoverOutput) = action {
		this.state = State.Detail(output.searchHit, state)
	}

	private fun handleToolbarOutput(output:ToolbarWorkflow.ToolbarOutput) = action {
		when (output) {
			//user has submitted a search query in the toolbar
			is ToolbarWorkflow.ToolbarOutput.SearchOutput -> {
				Plog.log("search submitted from toolbar: ${output.query}")
				this.state = State.Discover(output.query, state)
			}
		}
	}

	sealed class State(
		val name:String,
		open val previousState:State?
	) {
		data class Welcome(override val previousState:State? = null):State("Welcome", null)

		data class Discover(
			val query:String? = null,
			override val previousState:State? = null
		):State("Discover", previousState)

		data class Detail(
			val podcastInfo:SearchHit,
			override val previousState:State? = null
		):State("Detail", previousState)

		data class Subscriptions(override val previousState:State? = null):
			State("Subscriptions", previousState)
	}

	override fun initialState(props:Unit, snapshot:Snapshot?):State {
		return snapshot?.bytes?.parse { source ->
			snapshotter.deserialize( source.readString(snapshotter.charset), State::class.java)
		} ?: State.Welcome(previousState = null)
	}

	override fun snapshotState(state:State):Snapshot = Snapshot.write {sink->
		snapshotter.serialize(state, State::class.java)?.let{
			sink.writeString( it, snapshotter.charset )
		}
	}
}
