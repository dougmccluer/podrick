import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("com.android.application")
	kotlin("android")
	kotlin("android.extensions")
	kotlin("kapt")
}

android {
	compileSdkVersion(30)
	buildToolsVersion = "30.0.2"

	buildFeatures {
		viewBinding = true
	}

	defaultConfig {
		applicationId = "com.dougmccluer.podrick"
		minSdkVersion(21)
		targetSdkVersion(30)
		versionCode = 1
		versionName = "1.0"

		testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
	}

	buildTypes {
		named("release").configure {
			isMinifyEnabled = false
			proguardFiles(
				getDefaultProguardFile("proguard-android-optimize.txt"),
				"proguard-rules.pro"
			)
		}
	}

	sourceSets {
		getByName("main").res.srcDirs(
			"src/main/res/layouts/screens/",
			"src/main/res/layouts/views/",
			"src/main/res/"
		)
	}

	compileOptions {
		sourceCompatibility = JavaVersion.VERSION_1_8
		targetCompatibility = JavaVersion.VERSION_1_8
		isCoreLibraryDesugaringEnabled = true
	}
	kotlinOptions {
		jvmTarget = "1.8"
		languageVersion = "1.4"
		useIR = false
	}
}



dependencies {

	coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:1.1.5")

	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.2")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.4.2")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-rx2:1.4.2")

	implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.0")
	implementation("androidx.lifecycle:lifecycle-viewmodel-savedstate:2.3.0")
	implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.3.0")

	implementation("androidx.activity:activity-ktx:1.2.0")

	//Square Workflow - unidirectional data flow framework
	val workflowVersion = "1.0.0-alpha.12"
	implementation("com.squareup.workflow1:workflow-ui-core-android:$workflowVersion")
	implementation("com.squareup.workflow1:workflow-ui-modal-android:$workflowVersion")
	implementation("com.squareup.workflow1:workflow-ui-backstack-android:$workflowVersion")
	implementation("com.squareup.workflow1:workflow-rx2:$workflowVersion")

	//koin - dependency injection
	val koin_version = "2.2.2"
	implementation("org.koin:koin-core:$koin_version")
	implementation("org.koin:koin-androidx-scope:$koin_version")
	implementation("org.koin:koin-android-viewmodel:$koin_version")

	//Moshi - json serialization
	val moshi_version = "1.11.0"
	implementation("com.squareup.moshi:moshi:$moshi_version")
	implementation("com.squareup.moshi:moshi-kotlin:$moshi_version")
	implementation("com.squareup.moshi:moshi-adapters:$moshi_version")
	kapt("com.squareup.moshi:moshi-kotlin-codegen:$moshi_version")

	//OKHTTP & Retrofit - HTTP request management
	implementation("com.squareup.okhttp3:okhttp:4.9.0")
	implementation("com.squareup.okhttp3:logging-interceptor:4.9.0")
	implementation("com.squareup.retrofit2:retrofit:2.9.0")
	implementation("com.squareup.retrofit2:adapter-rxjava2:2.9.0")
	implementation("com.squareup.retrofit2:converter-moshi:2.9.0")

	//Rx - asynchronous observable streams
	implementation("io.reactivex.rxjava2:rxjava:2.2.20")
	implementation("io.reactivex.rxjava2:rxandroid:2.1.1")

	//Glide - image loading & caching
	implementation("com.github.bumptech.glide:glide:4.12.0")
	kapt("com.github.bumptech.glide:compiler:4.12.0")
	kapt("androidx.lifecycle:lifecycle-common-java8:2.3.0")

	//RSS-parsing
	implementation("com.ouattararomuald:syndication:1.1.0")

	implementation("org.jetbrains.kotlin:kotlin-stdlib:" + ProjectConfig.KOTLIN_VERSION)
	implementation("androidx.core:core-ktx:1.3.2")
	implementation("androidx.appcompat:appcompat:1.2.0")
	implementation("com.google.android.material:material:1.3.0")
	implementation("androidx.constraintlayout:constraintlayout:2.0.4")
	testImplementation("junit:junit:4.+")
	androidTestImplementation("androidx.test.ext:junit:1.1.2")
	androidTestImplementation("androidx.test.espresso:espresso-core:3.3.0")
	implementation(kotlin("reflect"))
}
