package ${PACKAGE_NAME}

import android.content.Context
import android.view.ViewGroup
import com.squareup.workflow1.ui.ViewEnvironment
import com.squareup.workflow1.ui.ViewFactory
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import com.squareup.workflow1.ui.bindShowRendering

//TODO: add this to the viewRegistry!

@OptIn(WorkflowUiExperimentalApi::class)
class ${NAME}(
):ViewFactory<${Screen_Name}Rendering> {

	override val type = ${Screen_Name}Rendering::class

	override fun buildView(
		initialRendering:${Screen_Name}Rendering,
		initialViewEnvironment:ViewEnvironment,
		contextForNewView:Context,
		container:ViewGroup?
	) = ${Screen_Name}View(context = contextForNewView)
		.apply {
			bindShowRendering(
				initialRendering,
				initialViewEnvironment
			) { rendering:${Screen_Name}Rendering, environment:ViewEnvironment ->
				showRendering(rendering, environment)
			}
		}
}