package ${PACKAGE_NAME}

import com.squareup.workflow1.Snapshot
import com.squareup.workflow1.StatefulWorkflow
import com.squareup.workflow1.ui.AndroidViewRendering
import com.squareup.workflow1.ui.ViewFactory
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi
import com.dougmccluer.podrick.util.IResourceProvider
import ${PACKAGE_NAME}.${NAME}.State

class ${NAME}(
    val resourceProvider:IResourceProvider
):StatefulWorkflow<Unit, State, ${Screen_Name}Output, ${Screen_Name}Rendering>() {
    
    sealed class State {
		object Default:State()
	}

	override fun initialState(props:Unit, snapshot:Snapshot?):State {
		return State.Default
	}

	override fun render(props:Unit, state:State, context:RenderContext):${Screen_Name}Rendering {
		return when (state) {
			State.Default -> {
				${Screen_Name}Rendering( resourceProvider = resourceProvider)
			}
		}
	}

	override fun snapshotState(state:State):Snapshot? {
		TODO("Not yet implemented")
	}
}

data class ${Screen_Name}Output(val output:Any)

@OptIn(WorkflowUiExperimentalApi::class)
class ${Screen_Name}Rendering(private val resourceProvider:IResourceProvider)
:AndroidViewRendering<${Screen_Name}Rendering> {
	override val viewFactory:ViewFactory<${Screen_Name}Rendering>
		get() = TODO("Not yet implemented")
}