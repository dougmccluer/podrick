package ${PACKAGE_NAME}

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import com.squareup.workflow1.ui.ViewEnvironment
import com.squareup.workflow1.ui.WorkflowUiExperimentalApi

@OptIn(WorkflowUiExperimentalApi::class)
class ${NAME}(
    context:Context
):FrameLayout(context) {

	private val binding = ${Screen_Name}ScreenBinding.inflate(LayoutInflater.from(context), this, true)

	fun showRendering (rendering:${Screen_Name}Rendering, env:ViewEnvironment){
		binding.apply {
			layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
			//TODO: use the data in rendering to set up the elements in rendering
		}
	}
}